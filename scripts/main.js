// Change image
let myImage = document.querySelector("img");

myImage.onclick = function () {
    let mySrc = myImage.getAttribute("src");
    if (mySrc === "images/wlop-6se.jpg") {
        myImage.setAttribute("src", "images/wlop-4se.jpg");
    } else {
        myImage.setAttribute("src", "images/wlop-6se.jpg");
    }
};

// Set user
let myButton = document.querySelector("button");
let myHeading = document.querySelector("h1");

function setArtist() {
    let artist = prompt("Please input the artist you want to visit:");
    
    if (!artist) {
        setArtist();
    } else {
        localStorage.setItem("artistName", artist);
        myHeading.textContent = "Artworks by " + artist;
    }
}

myButton.onclick = function () {
    setArtist();
};

// Init
if (!localStorage.getItem("artistName")) {
    setArtist();
} else {
    let storedName = localStorage.getItem("artistName");
    myHeading.textContent = "Artworks by " + storedName;
}
